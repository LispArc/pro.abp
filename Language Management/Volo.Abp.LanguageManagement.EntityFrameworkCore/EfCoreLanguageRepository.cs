﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Domain.Repositories.EntityFrameworkCore;
using Volo.Abp.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace Volo.Abp.LanguageManagement.EntityFrameworkCore
{
    public class EfCoreLanguageRepository : EfCoreRepository<ILanguageManagementDbContext, Language, Guid>, IReadOnlyBasicRepository<Language, Guid>, IReadOnlyBasicRepository<Language>, IBasicRepository<Language>, IBasicRepository<Language, Guid>, IRepository, ILanguageRepository
	{
		public EfCoreLanguageRepository(IDbContextProvider<ILanguageManagementDbContext> dbContextProvider)
			: base(dbContextProvider)
		{
		}

		public virtual async Task<List<Language>> GetListAsync(bool isEnabled)
		{
			return await DbSet.Where(p => p.IsEnabled == isEnabled).ToListAsync();
		}

		public async Task<List<Language>> GetListAsync(string sorting = null, int maxResultCount = 2147483647, int skipCount = 0, string filter = null)
		{
			var languages = await DbSet
				.WhereIf(!filter.IsNullOrWhiteSpace(), x => x.CultureName.Contains(filter))
				.OrderBy(sorting ?? "CreationTime desc")
				.PageBy(skipCount, maxResultCount)
				.ToListAsync();

			return languages;
		}
	}
}
