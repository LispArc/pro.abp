﻿using Volo.Abp.Application.Services;
using Volo.Abp.Localization;

namespace Volo.Abp.LanguageManagement
{
    public abstract class LanguageAppServiceBase : ApplicationService
	{
		protected LanguageAppServiceBase()
		{
			base.ObjectMapperContext = typeof(LanguageManagementApplicationModule);
			base.LocalizationResource = typeof(AbpLocalizationOptions);
		}
	}
}
