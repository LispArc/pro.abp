﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using Volo.Abp.Localization;

namespace Volo.Abp.LanguageManagement
{
    public class DynamicLocalizationResourceContributor : ILocalizationResourceContributor
	{
		protected LocalizationResource Resource;

		protected IDynamicResourceLocalizer DynamicResourceLocalizer;

		public void Initialize(LocalizationResourceInitializationContext context)
		{
			this.Resource = context.Resource;
			this.DynamicResourceLocalizer = context.ServiceProvider.GetRequiredService<IDynamicResourceLocalizer>();
		}

		public LocalizedString GetOrNull(string cultureName, string name)
		{
			return this.DynamicResourceLocalizer.GetOrNull(this.Resource, cultureName, name);
		}

		public void Fill(string cultureName, Dictionary<string, LocalizedString> dictionary)
		{
			this.DynamicResourceLocalizer.Fill(this.Resource, cultureName, dictionary);
		}
	}
}
