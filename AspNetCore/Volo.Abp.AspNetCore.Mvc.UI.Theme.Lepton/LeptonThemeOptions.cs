﻿namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton
{
    public class LeptonThemeOptions
	{
		public bool EnableDemoFeatures { get; set; }
	}
}
