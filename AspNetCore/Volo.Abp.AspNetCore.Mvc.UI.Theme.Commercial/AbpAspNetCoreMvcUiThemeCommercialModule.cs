﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Volo.Abp.AspNetCore.Mvc.UI.Bundling;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Bundling;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Commercial
{
    [DependsOn(
		typeof(AbpAspNetCoreMvcUiThemeSharedModule)
	)]
	public class AbpAspNetCoreMvcUiThemeCommercialModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			base.PreConfigure<IMvcBuilder>(mvcBuilder =>
			{
				mvcBuilder.AddApplicationPartIfNotExists(typeof(AbpAspNetCoreMvcUiThemeCommercialModule).Assembly);
			});
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpVirtualFileSystemOptions>(options =>
			{
				options.FileSets.AddEmbedded<AbpAspNetCoreMvcUiThemeCommercialModule>();
			});
			base.Configure<AbpBundlingOptions>(options =>
			{
				options.ScriptBundles.Configure(StandardBundles.Scripts.Global, configuration =>
				{
					configuration.AddContributors(new Type[]
					{
						typeof(CommercialThemeScriptContributor)
					});
				});
			});
		}
	}
}
