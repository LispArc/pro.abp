﻿using System;

namespace Volo.Abp.Account.Public.Web.Areas.Account.Controllers.Models
{
	public class AbpLoginResult
	{
		public AbpLoginResult(LoginResultType result)
		{
			this.Result = result;
		}

		public LoginResultType Result { get; }

		public string Description
		{
			get
			{
				return this.Result.ToString();
			}
		}
	}
}
