﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Volo.Abp.Emailing;
using Volo.Abp.Sms;
using Volo.Abp.Uow;

namespace Volo.Abp.Account.Public.Web.Pages.Account
{
	public class SendSecurityCodeModel : AccountPageModel
	{
		[BindProperty(SupportsGet = true)]
		[HiddenInput]
		public string ReturnUrl { get; set; }

		[HiddenInput]
		[BindProperty(SupportsGet = true)]
		public string ReturnUrlHash { get; set; }

		[BindProperty(SupportsGet = true)]
		[HiddenInput]
		public bool RememberMe { get; set; }

		public List<SelectListItem> Providers { get; set; }

		[BindProperty]
		public string SelectedProvider { get; set; }

		protected IEmailSender EmailSender { get; }

		protected ISmsSender SmsSender { get; }

		public SendSecurityCodeModel(IEmailSender emailSender, ISmsSender smsSender)
		{
			this.EmailSender = emailSender;
			this.SmsSender = smsSender;
		}

		[UnitOfWork]
		public virtual async Task<IActionResult> OnGetAsync()
		{
			var identityUser = await base.SignInManager.GetTwoFactorAuthenticationUserAsync();
			IActionResult result;
			if (identityUser == null)
			{
				result = this.RedirectToPage("./Login");
			}
			else
			{
				this.CheckCurrentTenant(identityUser.TenantId);
				var list = await base.UserManager.GetValidTwoFactorProvidersAsync(identityUser);
				this.Providers = (from userProvider in list
								  select new SelectListItem
								  {
									  Text = userProvider,
									  Value = userProvider
								  }).ToList<SelectListItem>();
				result = this.Page();
			}
			return result;
		}

		[UnitOfWork]
		public virtual async Task<IActionResult> OnPostAsync()
		{
			var user = await base.SignInManager.GetTwoFactorAuthenticationUserAsync();
			IActionResult result;
			if (user == null)
			{
				result = this.RedirectToAction("Login");
			}
			else
			{
				this.CheckCurrentTenant(user.TenantId);
				if (this.SelectedProvider != "GoogleAuthenticator")
				{
					string text = await base.UserManager.GenerateTwoFactorTokenAsync(user, this.SelectedProvider);
					var message = base.L["EmailSecurityCodeBody", new object[]
					{
						text
					}];
					if (this.SelectedProvider == "Email")
					{
						IEmailSender emailSender = this.EmailSender;
						await emailSender.SendAsync(await base.UserManager.GetEmailAsync(user), base.L["EmailSecurityCodeSubject"], message, true);
						emailSender = null;
					}
					else if (this.SelectedProvider == "Phone")
					{
						ISmsSender smsSender = this.SmsSender;
						await SmsSenderExtensions.SendAsync(smsSender, await base.UserManager.GetPhoneNumberAsync(user), message);
						smsSender = null;
					}
					message = null;
				}
				result = this.RedirectToPage("./VerifySecurityCode", new
				{
					provider = this.SelectedProvider,
					returnUrl = this.ReturnUrl,
					returnUrlHash = this.ReturnUrlHash,
					rememberMe = this.RememberMe
				});
			}
			return result;
		}
	}
}
