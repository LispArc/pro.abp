﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp.Application.Services;
using Volo.Abp.AspNetCore.Mvc;

namespace Volo.Abp.Account
{
	[Route("api/account-admin/settings")]
	[Area("accountAdmin")]
	[RemoteService(true, Name = AccountProAdminRemoteServiceConsts.RemoteServiceName)]
	public class AccountSettingsController : AbpController, IAccountSettingsAppService, IRemoteService, IApplicationService
	{
		protected IAccountSettingsAppService AccountSettingsAppService { get; }

		public AccountSettingsController(IAccountSettingsAppService accountSettingsAppService)
		{
			this.AccountSettingsAppService = accountSettingsAppService;
		}

		[HttpGet]
		public virtual async Task<AccountSettingsDto> GetAsync()
		{
			return await this.AccountSettingsAppService.GetAsync();
		}

		[HttpPut]
		public virtual async Task UpdateAsync(AccountSettingsDto input)
		{
			await this.AccountSettingsAppService.UpdateAsync(input);
		}
	}
}
