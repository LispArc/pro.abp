﻿namespace Volo.Abp.TextTemplateManagement.TextTemplates
{
    public class TextTemplateConsts
	{
		public static int MaxNameLength { get; set; }

		public static int MinCultureNameLength { get; set; }

		public static int MaxCultureNameLength { get; set; }

		public static int MaxContentLength { get; set; }

		static TextTemplateConsts()
		{
			MaxNameLength = 128;
			MinCultureNameLength = 1;
			MaxCultureNameLength = 10;
			MaxContentLength = 65535;
		}
	}
}
