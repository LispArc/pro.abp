﻿using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Volo.Abp.TextTemplateManagement.Localization;
using Volo.Abp.Validation;
using Volo.Abp.Validation.Localization;
using Volo.Abp.VirtualFileSystem;

namespace Volo.Abp.TextTemplateManagement
{
	[DependsOn(
		typeof(AbpValidationModule)
	)]
	public class TextTemplateManagementDomainSharedModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpVirtualFileSystemOptions>(options => options.FileSets.AddEmbedded<TextTemplateManagementDomainSharedModule>());
			base.Configure<AbpLocalizationOptions>(options =>
			{
				options.Resources
				.Add<TextTemplateManagementResource>("en")
				.AddBaseTypes(typeof(AbpValidationResource))
				.AddVirtualJson("/Volo/Abp/TextTemplateManagement/Localization/TextTemplateManagement");
			});
		}
	}
}
